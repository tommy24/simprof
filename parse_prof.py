#!/usr/bin/env python
################################
# Parsing different results
################################
import os
import re
import matplotlib
matplotlib.use('Agg', warn=True)
import matplotlib.pyplot as plt
import sys
import dcbench
import argparse
import xlsxwriter
import kmeans
from collections import OrderedDict
from collections import defaultdict
from collections import Counter
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_regression
import numpy as np
import tfidf
import plot
import scipy
import sampling
import math
from sklearn.decomposition import RandomizedPCA

def sanity_check():
    global args    
        
    if not args.exp:
        print('error: exp not defined')
        print("Usage: ./run_prof.py -run [RUN] -bench [BENCH]")
        exit(0)    

def process_options():
    parser = argparse.ArgumentParser(description='run_analytical.py')  

    parser.add_argument('-exp', action='store', dest='exp', default='test', help='Exp Name')    
    parser.add_argument('-bench', action='store', dest='bench', help='Benchmark Name')    
    parser.add_argument('-time', action='store', dest='time', default=str(1000), help='Running time')
    parser.add_argument('-int', action='store', dest='interval', default=str(1000), help='Interval')
    parser.add_argument('-trial', action='store', dest='trial', default="train", help='stage')
    parser.add_argument('-app', action='store', dest='app', default=str(0), help='approach')
    parser.add_argument('-load', action='store', dest='load', default=True, help='approach')
    return parser                                              

class ParseProf(object):
    def __init__(self):                
        self.mode = "perf"

    def second_sample(self, cf_lv):
        self.acc_file = open(os.path.join(self.dir_path, "results", "accuracy.txt"), "a")
        self.samp_file = open(os.path.join(self.dir_path, "results", "samp.txt"), "a")
        self.cache_file = open(os.path.join(self.dir_path, "results", "cache.txt"), "a")
        sample_nsecs = 0
        sample_insts = 0
        sample_cycles = 0
        sample_size = 0
        sample_ref = 0
        sample_miss = 0
        duration = math.pow(10, 10)
        for i in range(len(self.nsec_diffs)):            
            if sample_nsecs > duration:
                break            
            sample_nsecs += self.nsec_diffs[i]
            sample_insts += self.inst_diffs[i]
            sample_cycles += self.cycle_diffs[i]
            # sample_ref += self.ref_diffs[i]
            # sample_miss += self.miss_diffs[i]
            sample_size += 1

        oracle_CPI = sum(self.CPIs) / len(self.CPIs)
        # oracle_cache = sum(self.miss_rate) / len(self.miss_rate)
        sample_CPI = float(sample_cycles) / sample_insts
        # sample_cache = float(sample_miss) / sample_ref
        err = abs(sample_CPI - oracle_CPI) / (oracle_CPI)
        # err_cache = abs(sample_cache - oracle_cache) / oracle_cache
        # error        
        print >>self.acc_file, "SECOND", sample_size, err, len(self.CPIs), sample_CPI, oracle_CPI, 0, 0#, sample_insts, sample_cycles
        print >>self.samp_file, "SECOND", sample_size, err, len(self.CPIs), sample_CPI, oracle_CPI, 0, 0#, sample_insts, sample_cycles
        # print >>self.cache_file, "SECOND", err_cache
        self.acc_file.close()
        self.samp_file.close()
        
    def get_exp_name(self, s):
        i = s.rfind("_")
        return s[i + 1:]

    def get_bench_name(self, s):
        i = s.rfind("_")
        return s[:i]

    def ParseBench(self):
        self.exp = args.exp
        self.bench = args.bench
        self.trial = args.trial
        print "Parsing", self.bench, "stage", self.trial

        if self.trial == "train":            
            self.training_stage_strata()
        else:            
            self.testing_stage()            

    def ParseSuite(self):                
        for bench in dcbench.bigdatabench.keys():
            self.bench = bench                                    
            labels = self.training_stage()                    
            
        # Plot motivation data
        # self.write_xlsx(self.ipc_err, "motivation")

    def prediction(self, units, weights, labels):
        cf_lv = 0.997
        self.dir_path = "/home/hduser/simprof/results/prof/%s/%s/%s" % (args.exp, self.bench, self.trial)
        
        sampInstance = sampling.Sampling(self.bench, methods = self.feature_methods, events = self.events, miss_rates = self.miss_rates, mpki = self.MPKI, labels = self.labels, dir_path = self.dir_path, cf_lv = cf_lv)

        # get all phases
        total_phases = defaultdict(list)
        for i in range(len(self.labels)):
            pid = self.labels[i]
            if not pid in total_phases.keys():
                total_phases[pid] = []
            total_phases[pid].append(self.CPIs[i])

        # build model
        # sampInstance.build_gmm_model(total_phases)

        # Fit the model
        # sampInstance.aic_test(total_phases)
        sampInstance.t_test(total_phases)

    def get_avg_event_ref(self):
        total_cache_ref = []
        total_l1_ref = []
        total_CPI = []
        total_miss_rate = []
        
        prof_path = "/home/hduser/simprof/results/prof/%s/%s/%s/raw_1.txt" % (args.exp, self.bench, self.trial)
        if not os.path.exists(prof_path):
            print prof_path, "NOT FOUND!"
            exit(0)
            return

        self.prof_file = open(prof_path, 'r')
        true_rec = False
        for line in self.prof_file:
            if "PROF=" in line:                
                m = re.search(r'PROF=(\d+)', line)
                docid = int(m.group(1)) - 1
                # print "P", docid
                
                lineInfo = line.split()
                valid_cache_ref = 0
                valid_l1_ref = 0
                valid_CPI = 0
                valid_miss_rate = 0                
                for field in lineInfo:                        
                    col = field.split("=")
                    if "CPI" in col[0]:
                        # if not (float(col[1]) > 0 and float(col[1]) < 4):                                                    
                        #     true_rec = False
                        valid_CPI = float(col[1])
                    
                    elif "cache-ref" in col[0]:
                        valid_cache_ref = int(col[1])

                    elif "L1" in col[0]:
                        valid_l1_ref = int(col[1])

                    elif "cache-miss-rate" in col[0]:
                        if not (float(col[1]) > 0 and float(col[1]) < 1):                                                   
                            true_rec = False
                        valid_miss_rate = float(col[1])

                if true_rec:
                    total_cache_ref.append(valid_cache_ref)
                    total_l1_ref.append(valid_l1_ref)
                    total_CPI.append(valid_CPI)
                    total_miss_rate.append(valid_miss_rate)                

                true_rec = True
        
        self.prof_file.close()
        print sum(total_cache_ref), len(total_cache_ref), sum(total_l1_ref), len(total_l1_ref),sum(total_CPI), len(total_CPI),sum(total_miss_rate), len(total_miss_rate)        
        return sum(total_cache_ref) / len(total_cache_ref), sum(total_l1_ref) / len(total_l1_ref),sum(total_CPI) / len(total_CPI),sum(total_miss_rate) / len(total_miss_rate)        

    def prepare_method_vectors(self):
        prof_path = "/home/hduser/simprof/results/prof/%s/%s/%s/raw_1.txt" % (args.exp, self.bench, self.trial)
        if not os.path.exists(prof_path):
            print prof_path, "NOT FOUND!"
            exit(0)
            return        

        cur_docid = 0
        docid = 0
        cur_docsubid = 0
        cur_callstack = ""
        cur_sp_callstack = ""
        cur_sp_depth = 0
        cur_hw_vector = []
        cur_stacks = []

        self.callstacks = []                        
        self.hw_vectors = []
        self.CPIs = []
        self.events = []
        self.miss_rates = []
        self.MPKI = []
        self.ref_diffs = []
        self.miss_diffs = []
        self.nsec_diffs = []
        self.inst_diffs = []
        self.cycle_diffs = []
        self.total_lines = []
        self.oracle = 0
        lines = []
        true_rec = False
        CPI = 0
        miss_rate = 0
        total_insts = 0
        total_cycles = 0
        prev_level = ""
        MIN_LEVEL = 1
        MAX_LEVEL = 100

        # Get average cache references
        avg_cache_ref, avg_l1_ref, avg_CPI, avg_miss_rate = self.get_avg_event_ref()
        
        self.prof_file = open(prof_path, 'r')
        for line in self.prof_file:
            # print line
            lines.append(line)
            if "S=" in line and int(args.app) == 0:                                                 
                m = re.search(r'S=(\d+)\.(\d+)', line)
                # print line
                # if m == None:
                #     continue
                docid = int(m.group(1))
                docsubid = int(m.group(2))
                lineInfo = line.split()
                # if len(lineInfo) != 2:
                #     continue

                # New document is initiated
                if docid != cur_docid:                                        
                    cur_docid = docid
                    cur_docsubid = docsubid
                    cur_sp_callstack = "%s" % (lineInfo[1])
                    cur_sp_depth = 1
                    # Initialization
                    cur_callstack = ""
                    cur_stacks = []
                    prev_level = lineInfo[1]
                    
                # New sample is initiatied
                elif docsubid != cur_docsubid:                                 
                    # if len(cur_stacks) >= 5:
                    #     cur_callstack += " %s" % cur_stacks[-1]
                    #     cur_callstack += " %s" % cur_stacks[-2]
                    #     cur_callstack += " %s" % cur_stacks[-3]                        
                    #     cur_callstack += " %s" % cur_stacks[-4]
                    #     cur_callstack += " %s" % cur_stacks[-5]
                        # cur_callstack += " %s" % cur_stacks[-6]
                        # cur_callstack += " %s" % cur_stacks[-7]
                        # cur_callstack += " %s" % cur_stacks[-8]
                        # cur_callstack += " %s" % cur_stacks[-9]
                        # cur_callstack += " %s" % cur_stacks[-10]                                    
                    cur_sp_callstack = "%s" % (lineInfo[1])
                    cur_sp_depth = 1
                    cur_docsubid = docsubid
                    cur_stacks = []
                    prev_level = lineInfo[1]

                # More levels are coming, keep appending the document
                elif not ("iterator" in lineInfo[1].lower()):
                    # cur_callstack += "|%s" % (lineInfo[1])                    
                    # if lineInfo[1] != prev_level:
                    if not lineInfo[1] in cur_sp_callstack:                        
                        cur_sp_callstack += "|%s" % (lineInfo[1])                    
                        cur_sp_depth += 1
                    
                        cur_stacks.append(cur_sp_callstack)                                                   
                        if cur_sp_depth > MIN_LEVEL and cur_sp_depth < MAX_LEVEL:
                            no_line_info = 0
                            if "(" in lineInfo[1] and ")" in lineInfo[1]:
                                no_line_info = re.sub(r':\d+', '', lineInfo[1])                                
                            else:
                                no_line_info = lineInfo[1]
                                
                            # print no_line_info
                            # print lineInfo[1], no_line_info
                            cur_callstack += " %s" % (no_line_info)
                            # cur_callstack += " %s" % (lineInfo[1])
                            # cur_callstack += " %s" % (cur_sp_callstack)

                    prev_level = lineInfo[1]

            elif "V=" in line and int(args.app) == 1:
                lineInfo = line.split(',')                
                cur_callstack = lineInfo[1]
                m = re.search(r'V=(\d+),', line)
                cur_docid = int(m.group(1))                
                
            elif "PROF=" in line:
                true_rec = True
                cur_hw_vector = []
                m = re.search(r'PROF=(\d+)', line)
                docid = int(m.group(1))
                # print "P", docid
                event = {}
                
                lineInfo = line.split()                
                for field in lineInfo:
                    col = field.rsplit('=', 1)
                    event[col[0]] = int(col[1])                    

                # CPI = float(event["cpu_clk_unhalted:thread_p"]) / event["instructions"]
                CPI = float(event["cpu_clk_unhalted:thread_p"]) / event["INST_RETIRED:ANY_P"]

                if not "time" in event:
                    print line
                nsec_diff = event["time"]
                # inst_diff = event["instructions"]
                inst_diff = event["INST_RETIRED:ANY_P"]

                if inst_diff > 500000000:
                    true_rec = False
                    
                cycle_diff = event["cpu_clk_unhalted:thread_p"]

                # Check if this sample is legal
                # print "hihi", docid, cur_docid, args.app
                # if docid == cur_docid and cur_callstack != "" and true_rec == True:
                if true_rec == True:      
                    # self.hw_vectors.append(cur_hw_vector)                    
                    self.callstacks.append(cur_callstack)
                    self.CPIs.append(CPI)
                    self.events.append(event)                    
                    self.nsec_diffs.append(nsec_diff)
                    self.inst_diffs.append(inst_diff)                    
                    self.cycle_diffs.append(cycle_diff)
                    # self.ref_diffs.append(cache_ref)
                    # self.miss_diffs.append(cache_miss)
                    # self.miss_rate.append(miss_rate)
                    # self.MPKI.append(float(cache_miss) / inst_diff)
                    self.total_lines.append(lines)
                    lines = []
                                
                    # assert len(self.hw_vectors) == len(self.callstacks)
                    # print cur_callstack
                    # self.total_lines.append(line)                    
                        
            # elif "Total CPI" in line:
            #     lineInfo = line.split()                
            #     for field in lineInfo:                        
            #         col = field.split("=")               
            #         if "CPI" in col[0]:
            #             self.oracle = float(col[1])

            # if docid == cur_docid and cur_sp_depth > MIN_LEVEL and cur_sp_depth < MAX_LEVEL and true_rec == True:            

        # self.oracle = float(total_insts) / total_cycles

    def testing_stage(self):
        self.prepare_method_vectors()

        # Give it to tfidf for classification
        dir_path = "/home/hduser/simprof/results/prof/%s/%s/%s" % (args.exp, self.bench, self.trial)
        tfidfInstance = tfidf.tfidf(dir_path, self.bench, "test", self.CPIs)
        # tfidfInstance.vectorize_callstacks(self.callstacks)
        tfidfInstance.counting_callstacks(self.callstacks)        
        self.labels, units, weights = tfidfInstance.apply_classifier()
        # tfidfInstance.write_to_xlsx(stage="test")
        self.feature_methods = []

        # plot
        plot_path = "/home/hduser/simprof/results/prof/%s/%s/%s" % (args.exp, self.bench, self.trial)
        plotInstance = plot.plot(plot_path)        
        plotInstance.plotphase("CPI_sort_%s" %(self.bench), self.CPIs, self.labels, sort = True)
        plotInstance.plotphase("CPI_%s" % (self.bench), self.CPIs, self.labels, sort = False)
        # plotInstance.plotphase("miss_rate", self.miss_rate, self.labels, sort = True)        
        # plotInstance.plotphase(self.CPIs, self.labels, sort = False)

        # Prediction
        self.prediction(units, weights, self.labels)        

        # Write phase file
        i = 0
        cur_phase = -1
        phase_lines = []
        phase_file = None
        in_phase = True
        self.label_file = {}
        os.system("rm -rf /home/hduser/simprof/results/prof/%s/%s/%s/phases" % (args.exp, self.bench, self.trial))
        os.system("mkdir -p /home/hduser/simprof/results/prof/%s/%s/%s/phases" % (args.exp, self.bench, self.trial))
        for phase in weights.keys():
            # Write label file            
            labels_path = "/home/hduser/simprof/results/prof/%s/%s/%s/phases/%s-%d.txt" % (args.exp, self.bench, self.trial, self.trial, phase)
            self.label_file[phase] = open(labels_path, 'w')

        for idx in range(len(self.total_lines)):            
            cur_phase = self.labels[idx]
            for line in self.total_lines[idx]:
                self.label_file[cur_phase].write(line)                            

        for phase in weights.keys():
            self.label_file[phase].close()    
        

    def concate_array(self, X, Y):
        # new_arr
        # for i in range(0, len(arr1)):
        #     new_arr.append(arr1[i] + arr2[i])
        # np.zeros(())
        new_arr = np.zeros((X.shape[0], X.shape[1] + 2))
        new_arr[:, :X.shape[1]] = X
        new_arr[:, X.shape[1]:] = Y        
        return new_arr

    def do_sampling(self):        
        # SRS
        cf_lv = 0.997
        sample_size = 20
        sampInstance = sampling.Sampling(self.bench, methods = self.feature_methods, events = self.events, miss_rates = self.miss_rates, mpki = self.MPKI, labels = self.labels, dir_path = self.dir_path, cf_lv = cf_lv)
        
        sampInstance.set_code(self.callstack_vecs, sample_size)        
        sampInstance.set_ci(0.02, 0)                
        sampInstance.set_ci(0.05, 0)
        sampInstance.set_ci(0.10, 0)

        sampInstance.set_sec(sample_size, self.nsec_diffs, self.inst_diffs, self.cycle_diffs)    
        sampInstance.set_sz(sample_size)

        # get all phases
        total_phases = defaultdict(list)
        for i in range(len(self.labels)):
            pid = self.labels[i]
            if not pid in total_phases.keys():
                total_phases[pid] = []
            total_phases[pid].append(self.CPIs[i])                

        # Build model
        # sampInstance.build_gmm_model(total_phases)

        # sampInstance.set_ci(0.10, 0)
        # sampInstance.SE_SRS()
        # sampInstance.SE_STRATA()        

        # Strata Sampling        
        # sampInstance.set_ci(0.1, 0)
        # sampInstance.SE_SRS()
        # sampInstance.SE_STRATA()


        # Write CPI mean and variance to file
        sampInstance.cpi_variance()

        # phase bottlenecks
        # sampInstance.phase_bottlenecks()
        
        if args.exp == "backend":
            sampInstance.frontend_bottlenecks()
            # sampInstance.backend_bottlenecks()
        elif args.exp == "frontend":
            sampInstance.frontend_bottlenecks()
        else:
            sampInstance.backend_bottlenecks()
                
        # Close result files
        sampInstance.close_files()

        # Do second sample
        self.second_sample(cf_lv)
            

    def training_stage_strata(self):        
        self.prepare_method_vectors()
        
        # Vectorization
        self.dir_path = "/home/hduser/simprof/results/prof/%s/%s/%s" % (args.exp, self.bench, self.trial)
        tfidfInstance = tfidf.tfidf(self.dir_path, self.bench, "train", self.CPIs)
        
        if args.load != True:                        
            # self.callstack_vecs = tfidfInstance.vectorize_callstacks(self.callstacks)
            self.callstack_vecs = tfidfInstance.counting_callstacks(self.callstacks)
            print "Done Vectorization"

            # pca = RandomizedPCA(n_components=100)
            # self.callstack_vecs = pca.fit_transform(self.callstack_vecs)                        
            # tfidfInstance.set_feature_names(new_feature_names)
            
            np.save(os.path.join(self.dir_path, "pca"), self.callstack_vecs) 
        else:
            # self.callstack_vecs = tfidfInstance.vectorize_callstacks(self.callstacks)
            tfidfInstance.counting_callstacks(self.callstacks)
            self.callstack_vecs = np.load(os.path.join(self.dir_path, "pca.npy"))
            tfidfInstance.load_callstacks()                
            print "Done Load Vectors"

            # Save new feature names
            selector = SelectKBest(f_regression, k=100)
            self.callstack_vecs = selector.fit_transform(self.callstack_vecs, self.CPIs)
            print "Done PCA"
            
            new_feature_names = []            
            features = tfidfInstance.get_feature_names()
            for i in selector.get_support(indices=True):                
                new_feature_names.append(i)
            np.save(os.path.join(self.dir_path, "features"), new_feature_names)
            tfidfInstance.set_feature_indices(new_feature_names)
        
            kmeansInstance = kmeans.Kmeans(self.CPIs, self.bench)        
            # labels, units, weights, centroids = kmeansInstance.RunKmeans(new_arr)
            self.labels, units, weights, centroids = kmeansInstance.RunKmeans(self.callstack_vecs)
            print "Done Clustering"        

            # Build classifier
            tfidfInstance.set_groundtruth(self.labels, centroids)
            tfidfInstance.build_classifier()
            tfidfInstance.write_to_xlsx()
            self.feature_methods = tfidfInstance.get_methods()

            # plot
            plot_path = "/home/hduser/simprof/results/prof/%s/%s/%s" % (args.exp, self.bench, self.trial)
            plotInstance = plot.plot(plot_path)
            print len(self.labels), len(self.CPIs)
            plotInstance.plotphase("CPI_sort_%s" %(self.bench), self.CPIs, self.labels, sort = True)
            plotInstance.plotphase("CPI_%s" % (self.bench), self.CPIs, self.labels, sort = False)
            # plotInstance.plotphase("miss_rate", self.miss_rate, self.labels, sort = True)        

            # Write label file            
            i = 0
            cur_phase = -1
            phase_lines = []
            phase_file = None
            in_phase = True
            self.label_file = {}

            os.system("mkdir -p /home/hduser/simprof/results/prof/%s/%s/%s/phases" % (args.exp, self.bench, self.trial))
            os.system("rm -rf /home/hduser/simprof/results/prof/%s/%s/%s/phases/*" % (args.exp, self.bench, self.trial))        
            for phase in weights.keys():            
                labels_path = "/home/hduser/simprof/results/prof/%s/%s/%s/phases/%s-%d.txt" % (args.exp, self.bench, self.trial, self.trial, phase)
                self.label_file[phase] = open(labels_path, 'w')

            for idx in range(len(self.total_lines)):            
                cur_phase = self.labels[idx]
                for line in self.total_lines[idx]:
                    self.label_file[cur_phase].write(line)                            

            for phase in weights.keys():
                self.label_file[phase].close()

            # Sampling is done here
            self.do_sampling()            

#########################################################################################
# main function
#########################################################################################
def main(argv):
    global args
    
    # parse arguments
    parser = process_options()
    args = parser.parse_args()
    sanity_check()
    
    runInstance = ParseProf()        
    
    if args.bench != None:
        runInstance.ParseBench()
    else:
        runInstance.ParseSuite()    

if __name__ == '__main__':
    main(sys.argv)        
        
