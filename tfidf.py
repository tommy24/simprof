#!/usr/bin/env python
from sklearn import metrics
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.cluster import MiniBatchKMeans, KMeans
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.cluster import Ward
from math import ceil
from collections import OrderedDict
from collections import defaultdict
from scipy.sparse import csr_matrix
import numpy as np
import math
import os
import re
import sys
import argparse
import fnmatch
import shutil
import collections
import glob
import fastcluster
import matplotlib.pyplot as plt
import unsupervised_alt
import scipy.cluster.hierarchy as hac
import xlsxwriter

class tfidf(object):
    def __init__(self, dir_path, bench, stage, CPIs):        
        self.dir_path = dir_path
        self.bench = bench
        self.stage = stage
        self.CPIs = CPIs
        
        if stage == "test":
            upper_level = os.path.abspath(os.path.join(self.dir_path, os.pardir))
            self.phase_detector = np.load(os.path.join(upper_level, "%s_class.npy" % self.bench)).item()
            self.vocabulary = np.load(os.path.join(upper_level, "%s_voc.npy" % self.bench)).item()

    def set_groundtruth(self, truth_labels, centroids):
        self.truth_labels = truth_labels
        self.centroids = centroids

    def devectorization(self):
        callstacks = self.vectorizer.get_feature_names()

    def get_feature_names(self):
        return self.vectorizer.get_feature_names()

    def set_feature_indices(self, new_feature_names):
        self.new_feature_names = new_feature_names        

    def counting_callstacks(self, X):
        if self.stage == "train":
            self.vectorizer = CountVectorizer(min_df=1, token_pattern=u'\S+', max_features=1000)
        else:
            self.vectorizer = CountVectorizer(min_df=1, token_pattern=u'\S+', vocabulary=self.vocabulary)
        self.tfidf_arr = self.vectorizer.fit_transform(X).toarray()        
            
        return self.tfidf_arr
    

    def load_callstacks(self):
        self.tfidf_arr = np.load(os.path.join(self.dir_path, "pca.npy"))        
        
    def vectorize_callstacks(self, X):
        if self.stage == "train":
            self.vectorizer = TfidfVectorizer(min_df=1, use_idf=True, token_pattern=u'\S+')
        else:
            self.vectorizer = TfidfVectorizer(min_df=1, use_idf=True, token_pattern=u'\S+', vocabulary=self.vocabulary)
        self.tfidf_arr = self.vectorizer.fit_transform(X).toarray()        
        
        return self.tfidf_arr

    def build_classifier(self):
        self.phase_vectors = defaultdict(list)
        self.phase_detector = {}
        self.diff_detector = {}
        self.phase_weight = {}        
        total_weight = 0
        
        assert len(self.truth_labels) == len(self.tfidf_arr)
        for i in range(len(self.truth_labels)):
            phase = self.truth_labels[i]
            self.phase_vectors[phase].append(self.tfidf_arr[i])
            if not phase in self.phase_weight:
                self.phase_weight[phase] = 0
            self.phase_weight[phase] += 1
            total_weight += 1
        
        # build the classifier
        for phase in self.phase_vectors.keys():
            self.phase_detector[phase] = np.average(self.phase_vectors[phase], axis=0)
            self.diff_detector[phase] = self.phase_detector[phase]
            self.phase_weight[phase] /= float(total_weight)

        for cur_phase in self.phase_vectors.keys():
            for phase in self.phase_vectors.keys():
                if phase != cur_phase:
                    self.diff_detector[cur_phase] = np.subtract(self.diff_detector[cur_phase], self.phase_detector[phase])

        # Check
        # for i in range(len(self.truth_labels)):
        #     if self.truth_labels[i] == test_phase_id:
        #         max_n = 3
        #         ids = np.argsort(self.tfidf_arr[i])[::-1][:max_n]
        #         callstacks = self.vectorizer.get_feature_names()
        #         for stack_id in ids:
        #             # Get current stack            
        #             print test_phase_id, len(self.phase_vectors[test_phase_id]), callstacks[stack_id]
        
        for phase in self.phase_vectors.keys():
            for i in np.ndindex(self.phase_detector[phase].shape):            
                if not i[0] in self.new_feature_names:                
                    self.phase_detector[phase][i[0]] = 0        

        # save the classifier
        upper_level = os.path.abspath(os.path.join(self.dir_path, os.pardir))
        np.save(os.path.join(upper_level, "%s_class" % self.bench), self.phase_detector)                
        
        np.save(os.path.join(upper_level, "%s_voc" % self.bench), self.vectorizer.vocabulary_)
        
    def apply_classifier(self):        
        self.phase_weight = {}

        min_dist = 1000
        cur_phase = 0
        labels = []
        total_weight = 0
        phase_unit = defaultdict(list)
        # phase_dist = defaultdict(lambda : 1000)

        # Do classification
        for unit in range(len(self.tfidf_arr)):
            for phase, vec in self.phase_detector.iteritems():            
                dist = np.linalg.norm(self.tfidf_arr[unit] - vec)                
                
                if dist < min_dist:
                    cur_phase = phase
                    min_dist = dist
            labels.append(cur_phase)

            # Pick the center
            # if min_dist < phase_dist[cur_phase]:                
            phase_unit[cur_phase].append(unit)
            # phase_dist[cur_phase] = min_dist
            
            if not cur_phase in self.phase_weight:
                self.phase_weight[cur_phase] = 0
            self.phase_weight[cur_phase] += 1
            total_weight += 1            
            min_dist = 1000

        # Normalize weight
        for phase in self.phase_weight.keys():            
            self.phase_weight[phase] /= float(total_weight)
        
        return labels, phase_unit, self.phase_weight

    def get_methods(self):
        return self.feature_methods
                    
    def write_to_xlsx(self, stage="train"):
        # Create an new Excel file and add a worksheet.
        workbook = xlsxwriter.Workbook(os.path.join(self.dir_path, "%s.xlsx" % stage))
        worksheet = workbook.add_worksheet()
                
        header_format = workbook.add_format({'text_wrap': 1, 'valign': 'top'})                
        
        # test distance
        row = ["phase_id", "phase_weight", "callstacks"]
        for i in range(len(row)):
            worksheet.write(0, i, str(row[i]))
            
        num_rows = 1
        max_col_width = {}
        self.feature_methods = defaultdict(list)
        for phase in self.phase_weight.keys():
            if self.phase_weight[phase] < 0.01:
                continue
            
            # Check important features
            max_n = 3
            callstacks = self.vectorizer.get_feature_names()            
            
            # ids = np.argsort(self.phase_detector[phase])[::-1][:max_n]
            ids = np.argsort(self.diff_detector[phase])[::-1][:max_n]
            for stack_id in ids:                
                # Get current stack
                each_call = callstacks[stack_id].split('|')

                self.feature_methods[phase].append(each_call[0])                
                # print len(each_call), callstacks[stack_id]
                # Get current row
                if stage == "train":
                    row = [phase, "%.3f" % self.phase_weight[phase], "\n".join(each_call)]
                else:
                    row = [phase, "%.3f" % self.phase_weight[phase], "\n".join(each_call)]
                    
                # Write current row
                for col in range(len(row)):                    
                    worksheet.write(num_rows, col, str(row[col]), header_format)
                    worksheet.set_row(num_rows, len(each_call) * 15)
                
                # Set max column width for each column
                for col in range(len(row)):                     
                    if not col in max_col_width:
                        max_col_width[col] = 0                    
                        
                    if len(str(row[col])) > max_col_width[col]:                        
                        max_col_width[col] = len(str(row[col]))
                        if col == 3:
                            max_col_width[col] = len(str(row[col])) / len(each_call) * 1.2
                            
                # Get next row
                num_rows += 1

        for col in max_col_width:            
            worksheet.set_column(col, col, max_col_width[col] + 10)  
                    
        workbook.close()        
