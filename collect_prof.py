#!/usr/bin/env python
################################
# Collect different results
################################
import os
import re
import matplotlib
matplotlib.use('Agg', warn=True)
import matplotlib.pyplot as plt
import sys
import dcbench
import argparse
import xlsxwriter
import kmeans
from collections import OrderedDict
from collections import defaultdict
import numpy as np
import tfidf
import plot
import scipy
import sampling
from sklearn.decomposition import RandomizedPCA

ERROR_FIELD = 1
SIZE_FIELD = 2

def sanity_check():
    global args    
    
    if not args.exp:
        print('error: exp not defined')
        print("Usage: ./run_prof.py -run [RUN] -bench [BENCH]")
        exit(0)

def process_options():
    parser = argparse.ArgumentParser(description='run_analytical.py')  

    parser.add_argument('-exp', action='store', dest='exp', default='test', help='Exp Name')    
    parser.add_argument('-bench', action='store', dest='bench', help='Benchmark Name')    
    parser.add_argument('-time', action='store', dest='time', default=str(1000), help='Running time')
    parser.add_argument('-int', action='store', dest='interval', default=str(1000), help='Interval')
    parser.add_argument('-trial', action='store', dest='trial', default="train", help='stage')
    parser.add_argument('-app', action='store', dest='app', default=str(0), help='approach')
    return parser                                              

class CollectProf(object):
    def __init__(self):                        
        self.exp = args.exp
        self.bench = args.bench
        self.trial = args.trial

    def CollectBenchSample(self, bench_name, file_name):
        dir_path = "/home/hduser/simprof/results/prof/%s/" % (args.exp)        
                
        # Create a xlsx file for all results
        self.app = defaultdict(list)        

        result_path = "/home/hduser/simprof/results/prof/%s/%s/%s/results/" % (args.exp, bench_name, self.trial)
        
        for f in os.listdir(result_path):
            if not file_name == os.path.splitext(f)[0]:
                continue

            result = open(os.path.join(result_path, f), "r")            

            # Collect all fields
            for line in result:                    
                lineInfo = line.split(" ")
                category = lineInfo[0]                
                for field in lineInfo:
                    self.app[category].append(field)

            result.close()
            
    ###########################################
    # Exp - compare with simple random sampling
    ###########################################
    def CollectExpCompare(self):        
        dir_path = "/home/hduser/simprof/results/prof/%s/" % (args.exp)
        sample_size = defaultdict(list)
        for bench_name in os.listdir(dir_path):
            print bench_name
            if os.path.isdir(os.path.join(dir_path, bench_name)):                
                self.CollectBenchSample(bench_name, "samp")
                # rel_size = float(self.app["STRATA_0.05"][SIZE_FIELD]) / float(self.app["SRS_0.05"][SIZE_FIELD])
                # sample_size[bench_name].append(rel_size)
                rel_size = float(self.app["SECOND"][SIZE_FIELD])
                sample_size[bench_name].append(rel_size)                
                rel_size = float(self.app["SRS_0.02"][SIZE_FIELD])
                sample_size[bench_name].append(rel_size)                
                rel_size = float(self.app["STRATA_0.02"][SIZE_FIELD]) #/ float(self.app["SRS_0.02"][SIZE_FIELD])
                sample_size[bench_name].append(rel_size)                

        # Write to xlsx        
        xlsx_path = "/home/hduser/simprof/results/prof/%s/%s_sample.xlsx" % (args.exp, self.trial)
        col_names = ["Benchmark", "SECOND", "Random", "SimProf"]
        self.WriteXlsx(sample_size, col_names, xlsx_path)

    ###########################################
    # Exp - Compare the oracle IPC
    ###########################################
    def CollectOracleCompare(self):
        dir_path = "/home/hduser/simprof/results/prof/%s/" % (args.exp)
        sample_size = defaultdict(list)
        for bench_name in os.listdir(dir_path):
            print bench_name
            if os.path.isdir(os.path.join(dir_path, bench_name)):                
                self.CollectBenchSample(bench_name, "sensitivity")                
                rel_size = float(self.app["SECOND"][SIZE_FIELD])
                sample_size[bench_name].append(rel_size)                
                rel_size = float(self.app["SRS_0.02"][SIZE_FIELD])
                sample_size[bench_name].append(rel_size)                
                rel_size = float(self.app["STRATA_0.02"][SIZE_FIELD]) #/ float(self.app["SRS_0.02"][SIZE_FIELD])
                sample_size[bench_name].append(rel_size)                

        # Write to xlsx        
        xlsx_path = "/home/hduser/simprof/results/prof/%s/%s_sample.xlsx" % (args.exp, self.trial)
        col_names = ["Benchmark", "SECOND", "Random", "SimProf"]
        self.WriteXlsx(sample_size, col_names, xlsx_path)


    def CollectExpOracle(self):
        dir_path = "/home/hduser/simprof/results/prof/%s/" % (args.exp)
        sample_size = defaultdict(list)
        field_name = 1 # Error field
        for bench_name in os.listdir(dir_path):
            # if not "_sp" in bench_name:
            #     continue
            
            if os.path.isdir(os.path.join(dir_path, bench_name)):                
                self.CollectBenchSample(bench_name, "accuracy")                
                sample_size[bench_name].append(float(self.app["ORACLE"][field_name]))                

        # Write to xlsx
        xlsx_path = "/home/hduser/simprof/results/prof/%s/%s_oracle.xlsx" % (args.exp, self.trial)
        col_names = ["Benchmark", "ORACLE"]
        self.WriteXlsx(sample_size, col_names, xlsx_path)
        
    
    def CollectExpAccuracy(self):
        dir_path = "/home/hduser/simprof/results/prof/%s/" % (args.exp)
        sample_size = defaultdict(list)
        field_name = 2 # Error field
        for bench_name in os.listdir(dir_path):
            # if not "_sp" in bench_name:
            #     continue
            
            if os.path.isdir(os.path.join(dir_path, bench_name)):                
                self.CollectBenchSample(bench_name, "accuracy")                
                sample_size[bench_name].append(float(self.app["SECOND"][field_name]))
                sample_size[bench_name].append(float(self.app["SRS"][field_name]))
                sample_size[bench_name].append(float(self.app["Code"][field_name]))
                sample_size[bench_name].append(float(self.app["STRATA"][field_name]))                

        # Write to xlsx
        xlsx_path = "/home/hduser/simprof/results/prof/%s/%s_acc.xlsx" % (args.exp, self.trial)
        col_names = ["Benchmark", "SECOND", "SRS", "CODE", "STRATA"]
        self.WriteXlsx(sample_size, col_names, xlsx_path)        
        

    def CollectExpSecond(self, field_name):
        dir_path = "/home/hduser/simprof/results/prof/%s/" % (args.exp)
        sample_size = defaultdict(list)
        for bench_name in os.listdir(dir_path):
            if os.path.isdir(os.path.join(dir_path, bench_name)):                
                self.CollectBenchSample(bench_name, "samp")                
                sample_size[bench_name].append(float(self.app["STRATA_0.05"][field_name]))                
                sample_size[bench_name].append(float(self.app["STRATA_0.02"][field_name]))
                sample_size[bench_name].append(float(self.app["SRS_0.05"][field_name]))
                sample_size[bench_name].append(float(self.app["SRS_0.02"][field_name]))
                sample_size[bench_name].append(float(self.app["SECOND"][field_name]))

        # Write to xlsx
        xlsx_path = "/home/hduser/simprof/results/prof/%s/%s_second.xlsx" % (args.exp, self.trial)
        col_names = ["Benchmark", "SimProf_0.05", "SimProf_0.02", "SRS_0.05", "SRS_0.02", "SECOND"]        
        self.WriteXlsx(sample_size, col_names, xlsx_path)

    ##########################
    # CPI variance
    ##########################        
    def CollectExpStrata(self):
        dir_path = "/home/hduser/simprof/results/prof/%s/" % (args.exp)
        CPI_variance = defaultdict(list)
        field_name = 1
        sample_size = defaultdict(list)
        for bench_name in os.listdir(dir_path):
            if os.path.isdir(os.path.join(dir_path, bench_name)):                
                self.CollectBenchSample(bench_name, "strata")                
                sample_size[bench_name].append(float(self.app["pop_cov"][field_name]))
                sample_size[bench_name].append(float(self.app["avg_cov"][field_name]))
                sample_size[bench_name].append(float(self.app["max_cov"][field_name]))

        # Write to xlsx
        xlsx_path = "/home/hduser/simprof/results/prof/%s/%s_cov.xlsx" % (args.exp, self.trial)
        # col_names = ["Benchmark", "STRATA_0.05", "STRATA_0.02", "SECOND"]
        col_names = ["Benchmark", "Population", "Average", "Maximum"]
        self.WriteXlsx(sample_size, col_names, xlsx_path)

    ##########################
    # Overall Category
    ##########################        
    def CollectOverallCategory(self):
        dir_path = "/home/hduser/simprof/results/prof/%s/" % (args.exp)
        CPI_variance = defaultdict(list)
        field_name = 1
        overall = defaultdict(list)
        for bench_name in os.listdir(dir_path):
            if os.path.isdir(os.path.join(dir_path, bench_name)):                
                self.CollectBenchSample(bench_name, "bottlenecks")                
                idx = self.app["Map"].index("CPI")
                overall[bench_name].append(float(self.app["Map"][idx + 1]))
                overall[bench_name].append(float(self.app["Reduce"][idx + 1]))
                overall[bench_name].append(float(self.app["Sort"][idx + 1]))                
                overall[bench_name].append(float(self.app["IO"][idx + 1]))                

        # Write to xlsx
        xlsx_path = "/home/hduser/simprof/results/prof/%s/%s_IPC.xlsx" % (args.exp, self.trial)        
        col_names = ["Benchmark", "Map", "Reduce", "Sort", "IO"]
        self.WriteXlsx(overall, col_names, xlsx_path)    

    ##########################
    # IPC Category
    ##########################        
    def CollectIPCCategory(self):
        dir_path = "/home/hduser/simprof/results/prof/%s/" % (args.exp)
        CPI_variance = defaultdict(list)
        field_name = 1
        IPCs = defaultdict(list)
        for bench_name in os.listdir(dir_path):
            if os.path.isdir(os.path.join(dir_path, bench_name)):                
                self.CollectBenchSample(bench_name, "bottlenecks")
                print bench_name
                idx = self.app["Map"].index("CPI")
                IPCs[bench_name].append(float(self.app["Map"][idx + 1]))
                IPCs[bench_name].append(float(self.app["Reduce"][idx + 1]))
                IPCs[bench_name].append(float(self.app["Sort"][idx + 1]))                
                IPCs[bench_name].append(float(self.app["IO"][idx + 1]))                

        # Write to xlsx
        xlsx_path = "/home/hduser/simprof/results/prof/%s/%s_IPC.xlsx" % (args.exp, self.trial)        
        col_names = ["Benchmark", "Map", "Reduce", "Sort", "IO"]
        self.WriteXlsx(IPCs, col_names, xlsx_path)

    ##########################
    # Weight Category
    ##########################        
    def CollectWeightCategory(self):
        dir_path = "/home/hduser/simprof/results/prof/%s/" % (args.exp)
        CPI_variance = defaultdict(list)
        field_name = 1
        weights = defaultdict(list)
        for bench_name in os.listdir(dir_path):
            if os.path.isdir(os.path.join(dir_path, bench_name)):                
                self.CollectBenchSample(bench_name, "bottlenecks")                
                idx = self.app["Map"].index("weight")
                weights[bench_name].append(float(self.app["Map"][idx + 1]))
                weights[bench_name].append(float(self.app["Reduce"][idx + 1]))
                weights[bench_name].append(float(self.app["Sort"][idx + 1]))                
                weights[bench_name].append(float(self.app["IO"][idx + 1]))                

        # Write to xlsx
        xlsx_path = "/home/hduser/simprof/results/prof/%s/%s_weight.xlsx" % (args.exp, self.trial)        
        col_names = ["Benchmark", "Map", "Reduce", "Sort", "IO"]
        self.WriteXlsx(weights, col_names, xlsx_path)

    ##########################
    # Phase Category
    ##########################        
    def CollectPhaseCategory(self):
        dir_path = "/home/hduser/simprof/results/prof/%s/" % (args.exp)
        CPI_variance = defaultdict(list)
        field_name = 1
        phases = defaultdict(list)
        for bench_name in os.listdir(dir_path):
            if os.path.isdir(os.path.join(dir_path, bench_name)):                
                self.CollectBenchSample(bench_name, "bottlenecks")                
                idx = self.app["Map"].index("phases")
                phases[bench_name].append(float(self.app["Map"][idx + 1]))
                phases[bench_name].append(float(self.app["Reduce"][idx + 1]))
                phases[bench_name].append(float(self.app["Sort"][idx + 1]))                
                phases[bench_name].append(float(self.app["IO"][idx + 1]))                

        # Write to xlsx
        xlsx_path = "/home/hduser/simprof/results/prof/%s/%s_phases.xlsx" % (args.exp, self.trial)        
        col_names = ["Benchmark", "Map", "Reduce", "Sort", "IO"]
        self.WriteXlsx(phases, col_names, xlsx_path)

    ##########################
    # Breakdown Category
    ##########################        
    def CollectBreakdownCategory(self):
        dir_path = "/home/hduser/simprof/results/prof/%s/" % (args.exp)
        CPI_variance = defaultdict(list)
        field_name = 1
        breakdown = defaultdict(list)
        for bench_name in os.listdir(dir_path):
            if os.path.isdir(os.path.join(dir_path, bench_name)):                
                self.CollectBenchSample(bench_name, "bottlenecks")                
                idx = self.app["Map"].index("backend")

                idx_IPC = self.app["Map"].index("CPI")
                if not np.isnan(float(self.app["Map"][idx_IPC + 1])):
                    breakdown[bench_name].append(float(self.app["Map"][idx + 1]))

                if not np.isnan(float(self.app["Reduce"][idx_IPC + 1])):
                    breakdown[bench_name].append(float(self.app["Reduce"][idx + 1]))

                if not np.isnan(float(self.app["Sort"][idx_IPC + 1])):
                    breakdown[bench_name].append(float(self.app["Sort"][idx + 1]))

                if not np.isnan(float(self.app["IO"][idx_IPC + 1])):
                    breakdown[bench_name].append(float(self.app["IO"][idx + 1]))                                

        # Write to xlsx
        xlsx_path = "/home/hduser/simprof/results/prof/%s/%s_breakdown.xlsx" % (args.exp, self.trial)        
        col_names = ["Benchmark", "Map", "Reduce", "Sort", "IO"]
        self.WriteXlsx(breakdown, col_names, xlsx_path)

    ##########################
    # DTLB Category
    ##########################        
    def CollectDTLBCategory(self):
        dir_path = "/home/hduser/simprof/results/prof/%s/" % (args.exp)
        CPI_variance = defaultdict(list)
        field_name = 1
        DTLB = defaultdict(list)
        for bench_name in os.listdir(dir_path):
            if os.path.isdir(os.path.join(dir_path, bench_name)):                
                self.CollectBenchSample(bench_name, "bottlenecks")                
                idx = self.app["Map"].index("DTLB")                
                DTLB[bench_name].append(float(self.app["Map"][idx + 1]))                
                DTLB[bench_name].append(float(self.app["Reduce"][idx + 1]))                
                DTLB[bench_name].append(float(self.app["Sort"][idx + 1]))
                DTLB[bench_name].append(float(self.app["IO"][idx + 1]))                                                                

        # Write to xlsx
        xlsx_path = "/home/hduser/simprof/results/prof/%s/%s_DTLB.xlsx" % (args.exp, self.trial)        
        col_names = ["Benchmark", "Map", "Reduce", "Sort", "IO"]
        self.WriteXlsx(DTLB, col_names, xlsx_path)

    ##########################
    # ITLB Category
    ##########################        
    def CollectITLBCategory(self):
        dir_path = "/home/hduser/simprof/results/prof/%s/" % (args.exp)
        CPI_variance = defaultdict(list)
        field_name = 1
        ITLB = defaultdict(list)
        for bench_name in os.listdir(dir_path):
            if os.path.isdir(os.path.join(dir_path, bench_name)):                
                self.CollectBenchSample(bench_name, "bottlenecks")                                
                idx = self.app["Map"].index("ITLB")                
                ITLB[bench_name].append(float(self.app["Map"][idx + 1]))                
                ITLB[bench_name].append(float(self.app["Reduce"][idx + 1]))                
                ITLB[bench_name].append(float(self.app["Sort"][idx + 1]))
                ITLB[bench_name].append(float(self.app["IO"][idx + 1]))                                                                

        # Write to xlsx
        xlsx_path = "/home/hduser/simprof/results/prof/%s/%s_ITLB.xlsx" % (args.exp, self.trial)        
        col_names = ["Benchmark", "Map", "Reduce", "Sort", "IO"]
        self.WriteXlsx(ITLB, col_names, xlsx_path)

    ##########################
    # ICACHE Category
    ##########################        
    def CollectICACHECategory(self):
        dir_path = "/home/hduser/simprof/results/prof/%s/" % (args.exp)
        CPI_variance = defaultdict(list)
        field_name = 1
        ICACHE = defaultdict(list)
        for bench_name in os.listdir(dir_path):
            if os.path.isdir(os.path.join(dir_path, bench_name)):                
                self.CollectBenchSample(bench_name, "bottlenecks")                
                idx = self.app["Map"].index("ICACHE")                
                ICACHE[bench_name].append(float(self.app["Map"][idx + 1]))                
                ICACHE[bench_name].append(float(self.app["Reduce"][idx + 1]))                
                ICACHE[bench_name].append(float(self.app["Sort"][idx + 1]))
                ICACHE[bench_name].append(float(self.app["IO"][idx + 1]))                                                                

        # Write to xlsx
        xlsx_path = "/home/hduser/simprof/results/prof/%s/%s_ICACHE.xlsx" % (args.exp, self.trial)        
        col_names = ["Benchmark", "Map", "Reduce", "Sort", "IO"]
        self.WriteXlsx(ICACHE, col_names, xlsx_path)

    ##########################
    # L1 Category
    ##########################        
    def CollectL1Category(self):
        dir_path = "/home/hduser/simprof/results/prof/%s/" % (args.exp)
        CPI_variance = defaultdict(list)
        field_name = 1
        L1 = defaultdict(list)
        for bench_name in os.listdir(dir_path):
            if os.path.isdir(os.path.join(dir_path, bench_name)):                
                self.CollectBenchSample(bench_name, "bottlenecks")                
                idx = self.app["Map"].index("L1")                
                L1[bench_name].append(float(self.app["Map"][idx + 1]))                
                L1[bench_name].append(float(self.app["Reduce"][idx + 1]))                
                L1[bench_name].append(float(self.app["Sort"][idx + 1]))
                L1[bench_name].append(float(self.app["IO"][idx + 1]))                                                                

        # Write to xlsx
        xlsx_path = "/home/hduser/simprof/results/prof/%s/%s_L1.xlsx" % (args.exp, self.trial)        
        col_names = ["Benchmark", "Map", "Reduce", "Sort", "IO"]
        self.WriteXlsx(L1, col_names, xlsx_path)

    ##########################
    # L2 Category
    ##########################        
    def CollectL2Category(self):
        dir_path = "/home/hduser/simprof/results/prof/%s/" % (args.exp)
        CPI_variance = defaultdict(list)
        field_name = 1
        L2 = defaultdict(list)
        for bench_name in os.listdir(dir_path):
            if os.path.isdir(os.path.join(dir_path, bench_name)):                
                self.CollectBenchSample(bench_name, "bottlenecks")                
                idx = self.app["Map"].index("L2")                
                L2[bench_name].append(float(self.app["Map"][idx + 1]))                
                L2[bench_name].append(float(self.app["Reduce"][idx + 1]))                
                L2[bench_name].append(float(self.app["Sort"][idx + 1]))
                L2[bench_name].append(float(self.app["IO"][idx + 1]))                                                                

        # Write to xlsx
        xlsx_path = "/home/hduser/simprof/results/prof/%s/%s_L2.xlsx" % (args.exp, self.trial)        
        col_names = ["Benchmark", "Map", "Reduce", "Sort", "IO"]
        self.WriteXlsx(L2, col_names, xlsx_path)

    ##########################
    # L3 Category
    ##########################        
    def CollectL3Category(self):
        dir_path = "/home/hduser/simprof/results/prof/%s/" % (args.exp)
        CPI_variance = defaultdict(list)
        field_name = 1
        L3 = defaultdict(list)
        for bench_name in os.listdir(dir_path):
            if os.path.isdir(os.path.join(dir_path, bench_name)):                
                self.CollectBenchSample(bench_name, "bottlenecks")                
                idx = self.app["Map"].index("L3")                
                L3[bench_name].append(float(self.app["Map"][idx + 1]))                
                L3[bench_name].append(float(self.app["Reduce"][idx + 1]))                
                L3[bench_name].append(float(self.app["Sort"][idx + 1]))
                L3[bench_name].append(float(self.app["IO"][idx + 1]))                                                                

        # Write to xlsx
        xlsx_path = "/home/hduser/simprof/results/prof/%s/%s_L3.xlsx" % (args.exp, self.trial)        
        col_names = ["Benchmark", "Map", "Reduce", "Sort", "IO"]
        self.WriteXlsx(L3, col_names, xlsx_path)                    

    def WriteXlsx(self, lines, col_names, xlsx_path):
        # Create a workbook and add a worksheet.
        workbook = xlsxwriter.Workbook(xlsx_path)
        worksheet = workbook.add_worksheet()

        # Print column names
        for i in range(len(col_names)):
            worksheet.write(0, i, col_names[i])

        # Print row data
        row = 1
        col = 1
        for line_no in sorted(lines.keys()):
            worksheet.write(row, 0, line_no)
            for field in lines[line_no]:
                if np.isnan(field):
                    worksheet.write(row, col, 0)
                else:
                    worksheet.write(row, col, field)
                col += 1
            col = 1
            row += 1        

        workbook.close()

#########################################################################################
# main function
#########################################################################################
def main(argv):
    global args
    
    # parse arguments
    parser = process_options()
    args = parser.parse_args()
    sanity_check()
    
    runInstance = CollectProf()        

    runInstance.CollectIPCCategory()
    runInstance.CollectWeightCategory()
    runInstance.CollectPhaseCategory()
    runInstance.CollectBreakdownCategory()

    # if args.exp == "backend":
    #     runInstance.CollectL1Category()
    #     runInstance.CollectL2Category()
    #     runInstance.CollectL3Category()
    # else:
    #     runInstance.CollectDTLBCategory()
    #     runInstance.CollectITLBCategory()
    #     runInstance.CollectICACHECategory()
    runInstance.CollectExpAccuracy()
    runInstance.CollectExpSecond(1)
    runInstance.CollectExpOracle()
    

if __name__ == '__main__':
    main(sys.argv)        
    
        
