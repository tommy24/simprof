#!/usr/bin/env python
################################
# Running different profilers
################################
import matplotlib
matplotlib.use('Agg', warn=True)
import matplotlib.pyplot as plt
import os
import sys
import dcbench
import argparse
import subprocess
import time
import re
import kmeans
from collections import OrderedDict

def sanity_check():
    global args    
    
    if not args.run:
        print('error: run not defined')
        print("Usage: ./run_prof.py -run [RUN] -bench [BENCH]")
        exit(0)    

def process_options():
    parser = argparse.ArgumentParser(description='run_analytical.py')                                                                                                                 
    parser.add_argument('-run', action='store', dest='run', help='Profiling Type')
    parser.add_argument('-bench', action='store', dest='bench', help='Benchmark Name')    
    parser.add_argument('-time', action='store', dest='time', default=str(10), help='Interval')
    parser.add_argument('-int', action='store', dest='interval', default=str(1000), help='Interval')
    parser.add_argument('-name', action='store', dest='name', default='test', help='Interval')
    parser.add_argument('-x', action='store', dest='x', help='Interval')     
    return parser                                              

class SinglePhase(object):
    def __init__(self):
        pass

    def ParseBench(self):
        self.bench = args.bench
        print "Parsing", self.bench                        
        self.ParseRawInput()

    def ParseSuite(self):        
        self.ipc_err = OrderedDict()
        
        for bench in dcbench.bigdatabench.keys():
            self.bench = bench                        
            print "Parsing", self.bench                        
            self.ParseRawInput()

        # Plot motivation data
        # self.write_xlsx(self.ipc_err, "motivation")        
        

    def ParseRawInput(self):
        prof_path = "/home/hduser/simprof/results/prof/%s/hprof/%s" % (args.name, self.bench)
        if not os.path.exists(prof_path):
            print prof_path, "NOT FOUND!"
            exit(0)
            return

        for f in os.listdir(prof_path):
            cycles = []
            insts = []
            IPCs = []
            oracle_IPC = 0
            self.x = "full"
            self.filename = f.split(".")[0]            
            
            self.prof_file = open(os.path.join(prof_path, f), 'r')
            for line in self.prof_file:                
                if "IPC" in line and "Id" in line:
                    lineInfo = line.split()
                    for field in lineInfo:                        
                        col = field.split("=")
                        if "IPC" in col[0]:                            
                            IPCs.append(float(col[1]))
                        elif "insts" in col[0]:
                            # assert(int(col[1]) >= 0)
                            # assert(int(col[1]) < 5000000000)
                            if int(col[1]) < 0:
                                col[1] = 0
                            insts.append(int(col[1]))
                        elif "cycles" in col[0]:
                            # assert(int(col[1]) < 5000000000)
                            if int(col[1]) < 0:
                                col[1] = 0                                
                            cycles.append(int(col[1]))                            

            # Convert to vectors
            # vectors = self.GetVectors(IPCs, cycles)
            vectors = self.GetVectors(IPCs, insts)
            # print vectors
            # vectors = self.GetVectors(IPCs, IPCs)

            labels = self.PhaseId(vectors)
                        
            self.PlotResults(IPCs, cycles, labels, oracle_IPC)                

    def PhaseId(self, vectors):
        kmeansInstance = kmeans.Kmeans()
        labels = kmeansInstance.RunKmeans(vectors)

        return labels        

    def GetVectors(self, IPCs, cycles):
        vectors = []

        # Get average cycles
        avg_cycles = float(sum(cycles)) / len(cycles)
        
        for i in range(len(IPCs)):
            # vectors.append([IPCs[i], round(cycles[i] / avg_cycles, 2)])
            vectors.append([IPCs[i]])
        
        return vectors                

    def PlotResults(self, IPC, cycles, labels, oracle_IPC):
        plt.clf()
        plt.cla()
        plt.subplot(3,1,1)
        plt.ylabel("IPC")
        plt.axhline(y=oracle_IPC)
        plt.plot(range(len(IPC)), IPC, 'g.', markersize=2)

        plt.subplot(3,1,2)
        plt.ylabel("Cycles")
        plt.plot(range(len(cycles)), cycles, 'r.', markersize=2)

        plt.subplot(3,1,3)
        plt.ylabel("Labels")
        plt.plot(range(len(labels)), labels, 'b.', markersize=2)        
        
        plot_path = os.path.join("/home/hduser/simprof/results/plots", args.name, "hprof", self.bench)
        os.system("mkdir -p %s" % plot_path)        
        plt.savefig(os.path.join(plot_path, self.filename + ".eps"), format='eps', dpi=900)
        plt.clf()
        plt.cla()

#########################################################################################
# main function
#########################################################################################
def main(argv):
    global args

    # parse arguments
    parser = process_options()
    args = parser.parse_args()
    sanity_check()
    
    runInstance = SinglePhase()
        
    if args.bench != None:
        runInstance.ParseBench()
    else:        
        runInstance.ParseSuite()    

if __name__ == '__main__':
    main(sys.argv)        
