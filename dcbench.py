#!/usr/bin/python
import os
import sys

bigdatabench = {
    "sort_sp" : "cd $SIMPROF_HOME/benchmarks/BigDataBench_V3.1.1_Spark/MicroBenchmarks/;./run_MicroBenchmarks.sh 1 [set] [thread];",    
    "grep_sp" : "cd $SIMPROF_HOME/benchmarks/BigDataBench_V3.1.1_Spark/MicroBenchmarks/;./run_MicroBenchmarks.sh 2 [set] [thread];",    
    "wc_sp" : "cd $SIMPROF_HOME/benchmarks/BigDataBench_V3.1.1_Spark/MicroBenchmarks/;./run_MicroBenchmarks.sh 3 [set] [thread];",        
    "kmeans_sp"    : "cd $SIMPROF_HOME/benchmarks/BigDataBench_V3.1.1_Spark/SNS/Kmeans;./run_Kmeans.sh [set] [thread];",    
    "rank_sp"  : "cd $SIMPROF_HOME/benchmarks/BigDataBench_V3.1.1_Spark/SearchEngine/Pagerank;./run_Pagerank.sh [set] [thread];", 
    "cc_sp" : "cd $SIMPROF_HOME/benchmarks/BigDataBench_V3.1.1_Spark/SNS/Connected_Components;./run_connectComponents.sh [set] [thread];",
    "bayes_sp" : "cd $SIMPROF_HOME/benchmarks/BigDataBench_V3.1.1_Spark/E-commerce/Bayes;./run_naivebayes.sh [set] [thread];",
    "sort_hp" : "cd $SIMPROF_HOME/benchmarks/BigDataBench_V3.1_Hadoop_Hive/MicroBenchmarks/;./run_MicroBenchmarks.sh 1 [set];",    
    "grep_hp" : "cd $SIMPROF_HOME/benchmarks/BigDataBench_V3.1_Hadoop_Hive/MicroBenchmarks/;./run_MicroBenchmarks.sh 2 [set];",    
    "wc_hp" : "cd $SIMPROF_HOME/benchmarks/BigDataBench_V3.1_Hadoop_Hive/MicroBenchmarks/;./run_MicroBenchmarks.sh 3 [set];",        
    "kmeans_hp"    : "cd $SIMPROF_HOME/benchmarks/BigDataBench_V3.1_Hadoop_Hive/SNS/Kmeans;./run_Kmeans.sh [set];",    
    "rank_hp"  : "cd $SIMPROF_HOME/benchmarks/BigDataBench_V3.1_Hadoop_Hive/SearchEngine/PageRank;./run_PageRank.sh 17 [set];", 
    "cc_hp" : "cd $SIMPROF_HOME/benchmarks/BigDataBench_V3.1_Hadoop_Hive/SNS/Connected_Components;./run_connectedComponents.sh 17 [set];",
    "bayes_hp" : "cd $SIMPROF_HOME/benchmarks/BigDataBench_V3.1_Hadoop_Hive/E-commerce/;./run_naivebayes.sh [set];"
}


