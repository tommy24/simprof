#!/usr/bin/env python
################################
# Running different profilers
################################
import os
import sys
import dcbench
import argparse
import subprocess
import time
import re
import sysv_ipc

def sanity_check():
    global args    
        
    if not args.exp:
        print('error: exp not defined')
        print("Usage: ./run_prof.py -run [RUN] -bench [BENCH]")
        exit(0)    

def process_options():
    parser = argparse.ArgumentParser(description='run_analytical.py')
    parser.add_argument('-exp', action='store', dest='exp', default='test', help='Exp Name')    
    parser.add_argument('-bench', action='store', dest='bench', help='Benchmark Name')    
    parser.add_argument('-time', action='store', dest='time', default=str(0), help='Running time')
    parser.add_argument('-uint', action='store', dest='unit', default=str(10), help='Interval')
    parser.add_argument('-observation', action='store', dest='observation', default=str(10), help='Interval')
    parser.add_argument('-trial', action='store', dest='trial', default="full", help='Interval')
    parser.add_argument('-thread', action='store', dest='thread', default="1", help='Interval')    
    return parser                                              

class RunProf(object):    
    def __init__(self):
        self.prof_cmd = ""
        self.bench_cmd = ""
        self.bench = args.bench
        self.trial = args.trial
        self.thread = args.thread
        self.simprof_home = os.environ["SIMPROF_HOME"]    

    def setHprof(self, bench, exp):
        if "_sp" in bench:
            infile = open("/opt/spark-1.1.0/conf/spark-env.sh")
            outfile = open("/opt/spark-1.1.0/conf/spark-env.sh.bak", 'w')

            set_mapper = False
            set_reducer = False

            ########################################
            for line in infile:
                if "SPARK_EXECUTOR_OPTS=" in line:
                    line = re.sub(r"unit=(\S+),observation=(\S+),exp=(\S+),bench=(\S+)", "unit=%s,observation=%s,exp=%s,bench=%s/%s\"" % (args.unit, args.observation, exp, self.bench, self.trial), line)                
                    outfile.write(line)
                else:
                    outfile.write(line)
            ########################################  
            infile.close()
            outfile.close()

            os.system("mv /opt/spark-1.1.0/conf/spark-env.sh.bak /opt/spark-1.1.0/conf/spark-env.sh")
            
        else:
            infile = open("/opt/hadoop-1.0.2/conf/mapred-site.xml")
            outfile = open("/opt/hadoop-1.0.2/conf/mapred-site.xml.bak", 'w')

            set_mapper = False
            set_reducer = False

            ########################################
            for line in infile:
                if "hprof.so" in line:                    
                    line = re.sub(r"unit=(\S+),observation=(\S+),exp=(\S+),bench=(\S+)", "unit=%s,observation=%s,exp=%s,bench=%s/%s</value>" % (args.unit, args.observation, exp, self.bench, self.trial), line)                    
                    outfile.write(line)                    
                else:
                    outfile.write(line)
            ########################################            
            infile.close()
            outfile.close()

            os.system("mv /opt/hadoop-1.0.2/conf/mapred-site.xml.bak /opt/hadoop-1.0.2/conf/mapred-site.xml")
            
    
    def runCommand(self, cmd):
        if cmd != "":            
            process = subprocess.Popen(cmd, shell=True, executable='/bin/bash')

        return process
            
    def runProf(self, prof_cmd, bench_cmd = ""):
        bench_process = None
        prof_process = None

        os.system("ipcrm -M 0x42")
        
        # Run Benchmarks
        if bench_cmd != "":            
            os.system("mkdir -p /home/hduser/simprof/results/bench/%s" % (args.exp))            
            os.system("rm -rf /home/hduser/simprof/results/bench/%s/%s_%s.bench" % (args.exp, self.bench, self.trial))
            os.system("rm -rf /home/hduser/simprof/results/prof/%s/%s/%s/*" % (args.exp, self.bench, self.trial))
            print bench_cmd
            with open("/home/hduser/simprof/results/bench/%s/%s_%s.bench" % (args.exp, self.bench, self.trial), 'w') as out:
                bench_process = subprocess.Popen("time " + bench_cmd, stdout=out, stderr=out, shell=True, executable='/bin/bash', bufsize=1)            
                    
        # If the benchmark is still running, it should be good for profiling
        if bench_process.poll() != None:
            print self.bench, "failed!"
            return
        else:
            print self.bench, "continued!"            

        # periodic sleep
        elapsed_time = 0
        period = 1
        while elapsed_time < float(args.time) or float(args.time) == 0:
            if bench_process.poll() == None:
                elapsed_time += period
                time.sleep(period)                
            else:
                break        

        # kill perf and bench processes        
        if bench_process.poll() == None:
            bench_process.terminate()
            os.system("pkill -f SparkSubmit")

        os.system("ipcrm -M 0x42")
        
    def RunBench(self):
        print "Profiling", self.bench
        all_trials = ["train", "test1", "test2", "test3", "test4", "test5", "test6", "test7", "test8", "test9", "test10"]

        if self.trial == "full":
            for trial in all_trials:
                self.trial = trial
                bench_cmd = dcbench.bigdatabench[self.bench]        
                bench_cmd = re.sub(r"\[set\]", "%s" % (self.trial), bench_cmd)                
                bench_cmd = re.sub(r"\[thread\]", "%s" % (self.thread), bench_cmd)
                
                self.setHprof(self.bench, args.exp)
                self.runProf(self.prof_cmd, bench_cmd)            
            
        else:
            bench_cmd = dcbench.bigdatabench[self.bench]        
            bench_cmd = re.sub(r"\[set\]", "%s" % (self.trial), bench_cmd)
            bench_cmd = re.sub(r"\[thread\]", "%s" % (self.thread), bench_cmd)

            print bench_cmd
            self.setHprof(self.bench, args.exp)
            self.runProf(self.prof_cmd, bench_cmd)            

    def RunSuite(self):                
        for bench in dcbench.bigdatabench.keys():                        
            print "Profiling", bench
            self.bench = bench            

            bench_cmd = dcbench.bigdatabench[self.bench]
            self.setHprof(self.bench, args.exp)
            self.runProf(self.prof_cmd, bench_cmd)            

#########################################################################################
# main function
#########################################################################################
def main(argv):
    global args
    
    # parse arguments
    parser = process_options()
    args = parser.parse_args()
    sanity_check()
    
    runInstance = RunProf()    

    print "Exp Name is", args.exp
    
    if args.bench != None:
        print "bench", args.bench
        runInstance.RunBench()
    else:
        runInstance.RunSuite()    

if __name__ == '__main__':
    main(sys.argv)        
