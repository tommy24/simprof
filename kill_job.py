#!/usr/bin/env python
################################
# Running different profilers
################################
import os
import sys
import dcbench
import argparse
import subprocess
import time

non_mapreduce = ["ycsb", "caching"]

class KillJob(object):    
    def __init__(self):        
        #self.simprof_home = os.environ["SIMPROF_HOME"]
        pass

    def runCommand(self, cmd):
        if cmd != "":            
            process = subprocess.Popen(cmd, shell=True, executable='/bin/bash')

        return process
        
    def delHadoopJob(self):
        daemonCount = subprocess.check_output("hadoop job -list", shell=True)
        remain_jobs = False
        for line in daemonCount.split('\n'):
            if "job_" in line:
                # print "hadoop job -kill %s" % (line.split()[0])
                self.runCommand("hadoop job -kill %s\n" % (line.split()[0]))
                remain_jobs = True

        return remain_jobs
                

#########################################################################################
# main function
#########################################################################################
def main(argv):
    global args

    parser = argparse.ArgumentParser(description='run_analytical.py')    
    args = parser.parse_args()
    
    runInstance = KillJob()
    while runInstance.delHadoopJob() == True:
        time.sleep(10)
        continue
    # os.system("hadoop-daemon.sh stop jobtracker")
    # os.system("hadoop-daemon.sh start jobtracker")    

if __name__ == '__main__':
    main(sys.argv)        
