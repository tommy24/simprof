/*
 * Copyright (c) 2003, 2005, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * This source code is provided to illustrate the usage of a given feature
 * or technique and has been deliberately simplified. Additional steps
 * required for a production-quality application, such as security checks,
 * input validation and proper error handling, might not be present in
 * this sample code.
 */


#include "hprof.h"
#include "hprof_perf.h"
#include "perf_util.h"

typedef struct TlsInfo {
  jint            sample_status;      /* Thread status for cpu sampling */
  jboolean        agent_thread;       /* Is thread our own agent thread? */
  jthread         globalref;          /* Global reference for thread */
  Stack          *stack;              /* Stack of StackElements entry/exit */
  MonitorIndex    monitor_index;      /* last contended mon */
  jint            tracker_status;     /* If we are inside Tracker class */
  FrameIndex     *frames_buffer;      /* Buffer used to create TraceIndex */
  jvmtiFrameInfo *jframes_buffer;     /* Buffer used to create TraceIndex */
  int             buffer_depth;       /* Frames allowed in buffer */
  TraceIndex      last_trace;         /* Last trace for this thread */
  ObjectIndex     thread_object_index;/* If heap=dump */
  jlong           monitor_start_time; /* Start time for monitor */
  jint            in_heap_dump;       /* If we are an object in the dump */
} TlsInfo;

/* This file contains the cpu loop for the option cpu=samples */

/* The cpu_loop thread basically waits for gdata->sample_interval millisecs
 *   then wakes up, and for each running thread it gets their stack trace,
 *   and updates the traces with 'hits'.
 *
 * No threads are suspended or resumed, and the thread sampling is in the
 *   file hprof_tls.c, which manages all active threads. The sampling
 *   technique (what is sampled) is also in hprof_tls.c.
 *
 * No adjustments are made to the pause time or sample interval except
 *   by the user via the interval=n option (default is 10ms).
 *
 * This thread can cause havoc when started prematurely or not terminated
 *   properly, see cpu_sample_init() and cpu_term(), and their calls in hprof_init.c.
 *
 * The listener loop (hprof_listener.c) can dynamically turn on or off the
 *  sampling of all or selected threads.
 *
 */

/* Private functions */
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <sched.h>

static void JNICALL
cpu_loop_function(jvmtiEnv *jvmti, JNIEnv *env, void *p)
{
    int         loop_trip_counter;
    jboolean    cpu_loop_running;
    sigset_t new;
    int sig_id;
    int sample_count = 0;
    loop_trip_counter          = 0;

    gdata->agenttid = syscall(SYS_gettid);
    /////////////////////////
    // Set thread affinity
    /* cpu_set_t set; */
    /* CPU_ZERO(&set); */
    /* CPU_SET(3, &set); */
    /* sched_setaffinity(syscall(SYS_gettid), sizeof( cpu_set_t ), &set); */
    /////////////////////////

    gdata->cpu_simprof_lock = createRawMonitor("HPROF cpu simprof lock");
    // int i = set_representative(REP_KEY);    
    /* if (gdata->is_representative == 1) {       */
    /* cpu_sample_perf_start(env); */
    /* } */
    /* write_printf("i %d\n", getpid());     */    

    rawMonitorEnter(gdata->cpu_loop_lock); {
        gdata->cpu_loop_running = JNI_TRUE;
        cpu_loop_running = gdata->cpu_loop_running;
        /* Notify cpu_sample_init() that we have started */
        rawMonitorNotifyAll(gdata->cpu_loop_lock);
    } rawMonitorExit(gdata->cpu_loop_lock);

    rawMonitorEnter(gdata->cpu_sample_lock); /* Only waits inside loop let go */
    rawMonitorEnter(gdata->cpu_simprof_lock);

    jvmtiFrameInfo *frames;
    frames = (jvmtiFrameInfo *)malloc(gdata->max_trace_depth * sizeof(jvmtiFrameInfo));

    sigset_t sigset;
    int signum;
    struct timespec timeout;
    timeout.tv_sec = 1;
    timeout.tv_nsec = 0;    
    sigemptyset(&sigset);
    sigaddset(&sigset, SIGUSR2);
    
    while ( cpu_loop_running ) {              
        ++loop_trip_counter;                

        /* This is the normal short timed wait before getting a sample */        
        /* rawMonitorWait(gdata->cpu_sample_lock,  (jlong)1); */
        /* rawMonitorWait(gdata->cpu_sample_lock, 1); */        
        sigtimedwait(&sigset, NULL, &timeout);
        /* usleep(10); */
        
        if (gdata->myid != -1) {
          jint count = 0;
          jvmtiError errp;
          sigset_t sigset;
          char *method_name, *method_sig;
          jthread globalref;
          int i = 0, li = 0, lineNumber = 0;
          jclass klass;
          char *psname, *sfname;          
          char name[256];
          jint locationCount;          

          /* int locked = __sync_lock_test_and_set(&gdata->sig_lock[gdata->mycpu], 1); */
          /* if (locked) continue; */
                    
          globalref = get_globalref(env, gdata->id2jthread[gdata->myid]);
          getStackTrace(globalref, frames, gdata->max_trace_depth, &count);
          if (count >= 1) {
            getMethodName(frames[i].method, &method_name, &method_sig);            
            getMethodClass(frames[i].method, &klass);            
            getClassSignature(klass, &psname, NULL);
            char *start_class_str = strrchr(psname, '/');
            /* psname[strlen(psname) - 1] = 0; */
            
            // Save the last level methods
            sprintf(name, "%s::%s", start_class_str + 1, method_name);
            strcpy(gdata->total_methods[gdata->total_samples[gdata->myid] % gdata->observation], name);
            //

            i = count - 1;
            (*jvmti)->Deallocate(jvmti, (unsigned char *)method_name);
            (*jvmti)->Deallocate(jvmti, (unsigned char *)method_sig);
            (*jvmti)->Deallocate(jvmti, (unsigned char *)psname);            
            
            while (i >= (count - 50) && i >= 0 && gdata->myid != -1) {
              getMethodName(frames[i].method, &method_name, &method_sig);
              getMethodClass(frames[i].method, &klass);              
              getSourceFileName(klass, &sfname);                                                
              getClassSignature(klass, &psname, NULL);
              int lineNumber = getLineNumber(frames[i].method, frames[i].location);              
              start_class_str = strrchr(psname, '/');
              /* psname[strlen(psname) - 1] = 0; */              
              
              write_printf("S=%.2f-lv%d %s::%s(%s:%d)\n", (float)gdata->total_samples[gdata->myid] / gdata->observation + 1, count - 1 - i, start_class_str + 1, method_name, sfname, lineNumber);
              /* write_printf("S=%.2f-lv%d %s::%s\n", (float)gdata->total_samples[gdata->myid] / gdata->observation + 1, count - 1 - i, start_class_str + 1, method_name, sfname, lineNumber); */
              i --;

              (*jvmti)->Deallocate(jvmti, (unsigned char *)method_name);
              (*jvmti)->Deallocate(jvmti, (unsigned char *)method_sig);
              (*jvmti)->Deallocate(jvmti, (unsigned char *)psname);
              (*jvmti)->Deallocate(jvmti, (unsigned char *)sfname);              
            }

            if (gdata->myid != -1)
              io_flush();
                          
            /* (*jvmti)->Deallocate(jvmti, (jclass *)klass); */
          }
          
          __sync_lock_test_and_set(&gdata->myid, -1);
          /* __sync_lock_release(&gdata->sig_lock[gdata->mycpu]); */
        }   

        /* Make sure we really want to continue */
        rawMonitorEnter(gdata->cpu_loop_lock); {
            cpu_loop_running = gdata->cpu_loop_running;
        } rawMonitorExit(gdata->cpu_loop_lock);

        /* while(!__sync_bool_compare_and_swap(&gdata->cur_combo, 1, 0)) { */
          /* write_printf("2 gdata->cur_combo %d\n", gdata->cur_combo); */
          /* io_flush(); */
          /* sleep(1); */          
        /* }         */
        
        /* if (gdata->cur_combo == 0) */
        /*   continue;         */
        // if (!__sync_bool_compare_and_swap(&gdata->sig_lock, 0, 1)) {
        /* if (!gdata->sig_lock) { */
        /*   continue; */
        /* } */
        /* gdata->cur_combo = 0; */
        
        /* Break out if we are done */
        if ( !cpu_loop_running ) {
          break;
        }        
        
        /*
         * If a dump request came in after we checked at the top of
         * the while loop, then we catch that fact here. We
         * don't want to perturb the data that is being dumped so
         * we just ignore the data from this sampling loop.
         */
        /* rawMonitorEnter(gdata->dump_lock); { */
        /*     if (gdata->dump_in_process) { */
        /*         gdata->pause_cpu_sampling = JNI_TRUE; */
        /*     } */
        /* } rawMonitorExit(gdata->dump_lock); */

        /* Sample all the threads and update trace costs */        
        /* write_printf("test test\n");         */
        
        // tommy24 - additional command to dump data
        /* dump_all_data(env); */
        /* tls_sample_all_threads(env); */
        /* io_flush(); */
        /* sample_count ++; */
        /* gdata->total_samples ++; */
        /* write_printf("NONO\n"); */
        /* io_flush(); */

        /* Check to see if we need to finish */
        rawMonitorEnter(gdata->cpu_loop_lock); {
            cpu_loop_running = gdata->cpu_loop_running;
            rawMonitorNotifyAll(gdata->cpu_loop_lock);
        } rawMonitorExit(gdata->cpu_loop_lock);
    }
    
    free(frames);
    
    rawMonitorExit(gdata->cpu_sample_lock);
    rawMonitorExit(gdata->cpu_simprof_lock);

    rawMonitorEnter(gdata->cpu_loop_lock); {
        /* Notify cpu_sample_term() that we are done. */
        rawMonitorNotifyAll(gdata->cpu_loop_lock);
    } rawMonitorExit(gdata->cpu_loop_lock);
    
    LOG2("cpu_loop()", "clean termination");    
}

void
cpu_sample_perf_ipc(JNIEnv *env)
{

}


/* Perf Counting */
void
cpu_sample_perf_start(JNIEnv *env)
{    
  start_sig_mode();
}

void
cpu_sample_perf_read(JNIEnv *env)
{  
  read_proc_mode();  
}

void
cpu_sample_perf_stop(JNIEnv *env)
{    
  stop_sig_mode();
}

/* External functions */

void
cpu_sample_init(JNIEnv *env)
{
    gdata->cpu_sampling  = JNI_TRUE;
    
    cpu_sample_perf_start(env);     

    /* Create the raw monitors needed */
    gdata->cpu_loop_lock = createRawMonitor("HPROF cpu loop lock");
    gdata->cpu_sample_lock = createRawMonitor("HPROF cpu sample lock");

    /////////////////////////
    // Set thread affinity
    /* cpu_set_t set; */
    /* CPU_ZERO(&set); */
    /* CPU_SET(0, &set); */
    /* sched_setaffinity(syscall(SYS_gettid), sizeof( cpu_set_t ), &set); */
    /////////////////////////
      
    rawMonitorEnter(gdata->cpu_loop_lock); {
      createAgentThread(env, "HPROF cpu sampling thread",
                        &cpu_loop_function);
      /* Wait for cpu_loop_function() to notify us it has started. */
      rawMonitorWait(gdata->cpu_loop_lock, 0);
    } rawMonitorExit(gdata->cpu_loop_lock);
}

void
cpu_sample_off(JNIEnv *env, ObjectIndex object_index)
{
    jint count;

    count = 1;
    if (object_index != 0) {
        tls_set_sample_status(object_index, 0);
        count = tls_sum_sample_status();
    }
    if ( count == 0 ) {
        gdata->pause_cpu_sampling = JNI_TRUE;
    } else {
        gdata->pause_cpu_sampling = JNI_FALSE;
    }
}

void
cpu_sample_on(JNIEnv *env, ObjectIndex object_index)
{
    if ( gdata->cpu_loop_lock == NULL ) {
        cpu_sample_init(env);
    }

    if (object_index == 0) {
        gdata->cpu_sampling             = JNI_TRUE;
        gdata->pause_cpu_sampling       = JNI_FALSE;
    } else {
        jint     count;

        tls_set_sample_status(object_index, 1);
        count = tls_sum_sample_status();
        if ( count > 0 ) {
            gdata->pause_cpu_sampling   = JNI_FALSE;
        }
    }

    /* Notify the CPU sampling thread that sampling is on */
    rawMonitorEnter(gdata->cpu_sample_lock); {
        rawMonitorNotifyAll(gdata->cpu_sample_lock);
    } rawMonitorExit(gdata->cpu_sample_lock);

}

void
cpu_sample_term(JNIEnv *env)
{
  gdata->cpu_loop_running = JNI_FALSE;
  gdata->pause_cpu_sampling   = JNI_FALSE;    
    
  rawMonitorEnter(gdata->cpu_sample_lock); {
    /* Notify the CPU sampling thread to get out of any sampling Wait */
    rawMonitorNotifyAll(gdata->cpu_sample_lock);
  } rawMonitorExit(gdata->cpu_sample_lock);
  rawMonitorEnter(gdata->cpu_loop_lock); {
    if ( gdata->cpu_loop_running ) {
      gdata->cpu_loop_running = JNI_FALSE;
      /* Wait for cpu_loop_function() thread to tell us it completed. */
      rawMonitorWait(gdata->cpu_loop_lock, 0);
    }
  } rawMonitorExit(gdata->cpu_loop_lock);

  int i;
  int num_cpus = sysconf(_SC_NPROCESSORS_ONLN);
  for (i = 0; i < num_cpus; i ++) {  
    __sync_lock_test_and_set(&gdata->mycpu, i);
    write_printf("Total CPI=%.2f insts=%lld cycles=%lld\n",                 
                 (float)gdata->total_cycles[i] / gdata->total_insts[i],
                 gdata->total_insts[i],
                 gdata->total_cycles[i]);
    io_flush();
  }  
}
