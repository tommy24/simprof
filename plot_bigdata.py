#!/usr/bin/env python
import os
import matplotlib
matplotlib.use('GTKAgg', warn=True)
import matplotlib.pyplot as plt
import sys

# matplotlib.use('GTKAgg')
# Parse raw data
bench_name = sys.argv[1]
raw_path = os.path.join("/home/hduser/bigdatasim/results/data", bench_name + ".ipc")
raw_file = open(raw_path, 'r')

cycles = []
insts = []
cache_ref = []
cache_miss = []

for line in raw_file:
    if "not" in line:
        continue
    
    if "cycles" in line:
        lineInfo = line.split()
        cycles.append(int(lineInfo[1].replace(',','')))
        
    elif "instructions" in line:
        lineInfo = line.split()
        insts.append(int(lineInfo[1].replace(',','')))
    
    elif "cache-references" in line:
        lineInfo = line.split()
        cache_ref.append(int(lineInfo[1].replace(',','')))

    elif "cache-misses" in line:
        lineInfo = line.split()
        cache_miss.append(int(lineInfo[1].replace(',','')))

print len(cycles), len(insts), len(cache_ref), len(cache_miss)
assert len(cycles) == len(insts) == len(cache_ref) == len(cache_miss)

IPC = [float(insts[i]) / cycles[i] for i in range(len(insts))]
miss_rate = [float(cache_miss[i]) / cache_ref[i] for i in range(len(insts))]
MPKI = [float(cache_ref[i]) * 1000 / insts[i] for i in range(len(insts))]

# The oracle info
oracle_IPC = float(sum(insts)) / sum(cycles)
print bench_name, oracle_IPC, IPC[0]

plt.subplot(3,1,1)
plt.ylabel("IPC")
plt.plot(range(len(IPC)), IPC, 'g.', markersize=2)

plt.subplot(3,1,2)
plt.ylabel("Miss Rate")
plt.plot(range(len(miss_rate)), miss_rate, 'r.', markersize=2)

plt.subplot(3,1,3)
plt.ylabel("MPKI")
plt.plot(range(len(MPKI)), MPKI, 'b.', markersize=2)

# plt.show()
plt.savefig(os.path.join("/home/hduser/bigdatasim/results/plots", bench_name + ".png"))
