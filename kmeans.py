#!/usr/bin/env python
from sklearn import metrics
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.cluster import MiniBatchKMeans, KMeans
from sklearn.feature_extraction import DictVectorizer
from sklearn.cluster import Ward
from math import ceil
from collections import OrderedDict
from collections import defaultdict
from sklearn.feature_extraction import DictVectorizer
from scipy.sparse import csr_matrix
import numpy as np
import math
import os
import re
import sys
import argparse
import fnmatch
import shutil
import collections
import glob
import fastcluster
import matplotlib.pyplot as plt
import unsupervised_alt
import scipy.cluster.hierarchy as hac
from collections import Counter
import scipy
import milk
import gc

# class cluster_node:
#     def __init__(self,vec,left=None,right=None,distance=0.0,id=None,count=1):
#         self.left=left
#         self.right=right
#         self.vec=vec
#         self.id=id
#         self.distance=distance
#         self.count=count #only used for weighted average 

# def process_options():    
#     parser = argparse.ArgumentParser(description='parse_counters.py')
#     parser.add_argument('-loadFVFile', action='store', dest='loadFVFile', help='input directory')
#     # parser.add_argument('-bicThreshold', action='store', dest='bicThreshold', help='input directory')    
#     parser.add_argument('-exp', action='store', dest='exp', help='benchmark to pick simulation points')
#     parser.add_argument('-hwconfig', action='store', dest='hwconfig', help='benchmark to pick simulation points')
#     parser.add_argument('-bench', action='store', dest='bench', help='input directory')    
#     return parser


class Kmeans(object):
    def __init__(self, IPCs, bench):
        self.labels = []
        self.IPCs = IPCs
        self.bench = bench
        
    def GetMinDist(self, X, labels, centroids):
        minIdx = [0] * (labels.max() - labels.min() + 1)
        minDist = [0] * (labels.max() - labels.min() + 1)
        numUnits = [0] * (labels.max() - labels.min() + 1)
        
        # for i in range(len(X)):
        for i in range(X.shape[0]):
            if labels[i] >= len(minIdx):
                print labels[i], minIdx
            minIdx[labels[i]] = 0
            minDist[labels[i]] = 100000
            numUnits[labels[i]] = 0
            
        # for i in range(len(X)):
        for i in range(X.shape[0]):
            numUnits[labels[i]] += 1
            # print abs(X[i] - self.centroids[labels[i]])
            dist = np.linalg.norm(X[i] - centroids[labels[i]])            
            # print X[i], self.centroids[labels[i]]
            # if abs(X[i] - self.centroids[labels[i]]) < minDist[labels[i]]:
            if dist < minDist[labels[i]]:
                minIdx[labels[i]] = i
                minDist[labels[i]] = dist

        return minIdx, numUnits

    def RunHier(self, X):        
        X = np.array(X)
        z = fastcluster.linkage(X, method='ward', metric='euclidean')

        for i in range(3, 11):
            labels = hac.fcluster(z, i, 'maxclust')
            score = unsupervised_alt.silhouette_score_block(X, labels)
            labels = labels.tolist()        
        return labels


    def weighted_CoV(self, labels):
        weighted_CoV = 0

        assert len(self.IPCs) == len(labels)
        # Collect phase units
        phase_units = defaultdict(list)
        total_units = len(labels)
        for i in range(len(labels)):            
            phase_units[labels[i]].append(self.IPCs[i])            
        
        # weighted IPC
        program_CoV = scipy.stats.variation(self.IPCs)
        for phase_id in phase_units.keys():
            weight = float(len(phase_units[phase_id])) / total_units
            CoV = scipy.stats.variation(phase_units[phase_id])
            weighted_CoV += weight * CoV

        return weighted_CoV    

    def RunFixedKmeans(self, X, n):
        np.set_printoptions(threshold=np.nan)
        # X = np.array(X)
        allScores = []
        allLabels = []
        bestUnits = []
        bestCentroids = []
        clusterDist = []                        
        i = n        
        # if score < 0.9 and i < len(self.X):
        # while i <= len(X) and i < 11:

        k_means = MiniBatchKMeans(init='k-means++', n_clusters=i, batch_size=5000)
        k_means.fit_predict(X)            
        labels = k_means.labels_            
        centroids = k_means.cluster_centers_                    

        minIdx, numUnits = self.GetMinDist(X, labels, centroids)

        # Get phase weight
        weights = Counter(labels)
        for phase in weights.keys():
            weights[phase] /= float(len(labels))
            
        return minIdx, weights
    
    def RunKmeans(self, X):        
        np.set_printoptions(threshold=np.nan)
        # X = np.array(X)
        allScores = []
        allLabels = []
        bestUnits = []
        bestCentroids = []
        clusterDist = []

        if self.bench == "sort_hp" or self.bench == "wc_hp":
            i = 3        
        else:
            i = 2
            
        print "points", len(X), X.size
        # if score < 0.9 and i < len(self.X):
        # while i <= len(X) and i < 11:
        while i < 20:        
        # while i <= len(X) and i < 3:            
            # k_means = KMeans(init='k-means++', n_clusters=i, n_jobs=-1, precompute_distances=False)
            k_means = MiniBatchKMeans(init='k-means++', n_clusters=i, batch_size=5000)
            k_means.fit_predict(X)            
            labels = k_means.labels_            
            centroids = k_means.cluster_centers_

            # print "Done kmeans"        
            # labels, centroids = milk.kmeans(X, i, max_iter=10000)

            # Get Score
            # score = 1.0 - self.weighted_CoV(labels)                
                        
            score = unsupervised_alt.silhouette_score_block(X, labels, sample_size=10000)
            # score = metrics.silhouette_score(X, labels, metric='euclidean', samp)            
                
            print "score", score, i
            
            allLabels.append(labels)            
            
            # minIdx, numUnits = self.GetMinDist(X, labels, centroids)
            allScores.append(score)
            # bestUnits.append(minIdx)
            bestUnits.append(0)
            bestCentroids.append(centroids)
            i += 1
                        
            # if score >= 0.8: break                
            # print labels                
        
        # best_i = allScores.index(max(allScores))                
        
        best_i = -1
        # Select best i
        sil_threshold = max(allScores) * 0.9
        for i in range(len(allScores)):
            if allScores[i] > sil_threshold:
                best_i = i
                break

        if best_i == -1:
            best_i = allScores.index(max(allScores))

        print "best", best_i + 3

        # Get phase weight
        weights = Counter(allLabels[best_i])
        for phase in weights.keys():
            weights[phase] /= float(len(allLabels[best_i]))        
        
        return allLabels[best_i], bestUnits[best_i], weights, bestCentroids[best_i]
        # print bestUnits[runNum]                
